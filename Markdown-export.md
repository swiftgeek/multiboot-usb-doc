### Preparing pendrive {#preparing_pendrive}

Prepare pendrive with mbr/msdos partition scheme, with FAT32/VFAT
formatted partition.

#### Installing grub to pendrive {#installing_grub_to_pendrive}

#### Sample grub.cfg {#sample_grub.cfg}

Update this config with **UUID** for FAT32 partition (**A123-B321** in
this example) and **label** (**PenLabelHere** in this example). This
information can be found out by running **blkid** command with root
rights. This file should be saved as **/boot/grub/grub.cfg** inside of
your pendrive.

```
# Set default root for most targets
search -n -u --hint=hd0,msdos1 --set=root A123-B321
if [ -z ${root} ]; then
  search -n -l --hint=hd0,msdos1 --set=root PenLabelHere
  if [ -z ${root} ]; then
    set root (hd0,msdos1)
  fi
fi

probe -u $root --set=rootuuid
set imgdevpath="/dev/disk/by-uuid/$rootuuid"

# Per distro menuentries go here

menuentry "Reboot" {
  reboot
}

menuentry "Shutdown" {
  halt
}
```


### Distro menuentry {#distro_menuentry}

#### Alpine Linux {#alpine_linux}

`menuentry '[loopback]alpine x86_64' {`\
`       set isofile='/boot/iso/alpine-extended-3.6.0-x86_64.iso'`\
`       loopback loop $isofile`\
`       set root=loop`\
`       linux /boot/vmlinuz-hardened modloop=/boot/modloop-grsec modules=loop,squashfs,sd-mod,usb-storage quiet`\
`       initrd /boot/initramfs-hardened`\
`}`

#### CentOS

##### Stock installation medium {#stock_installation_medium}

`menuentry "[loopback]CentOS-7.0-1406-x86_64-`**`DVD`**`" {`\
`   set isofile='/boot/iso/CentOS-7.0-1406-x86_64-`**`DVD`**`.iso'`\
`   loopback loop $isofile`\
`   linux (loop)/isolinux/vmlinuz noeject inst.stage2=hd:`**`/dev/sdb2`**`:/$isofile`\
`   initrd (loop)/isolinux/initrd.img`\
`}`

##### Desktop live medium {#desktop_live_medium}

`menuentry '[loopback]CentOS-7.0-1406-x86_64-GnomeLive' {`\
`   set isofile='/boot/iso/CentOS-7.0-1406-x86_64-GnomeLive.iso'`\
`   loopback loop $isofile`\
`   linux (loop)/isolinux/vmlinuz0 root=live:CDLABEL=CentOS-7-live-GNOME-x86_64 iso-scan/filename=$isofile rd.live.image`\
`   initrd (loop)/isolinux/initrd0.img`\
`}`

#### Clonezilla Live {#clonezilla_live}

`menuentry "[loopback]clonezilla-live-20170220-yakkety-amd64" {`\
`   set isofile="/boot/iso/clonezilla-live-20170220-yakkety-amd64.iso"`\
`   loopback loop $isofile`\
`   linux (loop)/live/vmlinuz boot=live union=overlay username=user config components quiet noswap nolocales edd=on nomodeset ocs_live_run=\"ocs-live-general\" ocs_live_extra_param=\"\" keyboard-layouts= ocs_live_batch=\"no\" locales= vga=788 ip=frommedia nosplash toram=filesystem.squashfs findiso=$isofile`\
`   initrd (loop)/live/initrd.img`\
`}`

#### Debian live {#debian_live}

Read man live-boot-doc for more debian live specific arguments
<https://manpages.debian.org/testing/live-boot-doc/live-boot.7.en.html>
. Installer inside live was removed for stretch and later (classic
installer is available in /d-i/ of iso)

`menuentry "[loopback] Debian live" {`\
`   set isofile='/live/debian-live/debian-live-9.4.0-amd64-xfce.iso'`\
`   loopback loop ($root)$isofile`\
`   linux (loop)/live/vmlinuz* boot=live components findiso=$isofile`\
`   initrd (loop)/live/initrd*`\
`}`

##### Debian live - installer {#debian_live___installer}

**untested**

`menuentry "[loopback] Debian live - installer" {`\
`   set isofile='/live/debian-live/debian-live-9.4.0-amd64-xfce.iso'`\
`   loopback loop ($root)$isofile`\
`   linux (loop)/d-i/gtk/vmlinuz iso-scan/ask_second_pass=true iso-scan/filename=$isofile`\
`   initrd (loop)/d-i/gtk/initrd.gz`\
`}`

#### Debian netinstall {#debian_netinstall}

Download hd-media **initrd** for desired installer (textmode/gtk) from
**Tiny CDs, flexible USB sticks, etc.** section on
<https://www.debian.org/distrib/netinst>

`menuentry '[loopback] debian installer' {`\
`   set isofile='/debian-8.5.0-amd64-netinst.iso'`\
`   loopback loop $isofile`\
`   linux (loop)/install.amd/vmlinuz iso-scan/ask_second_pass=true iso-scan/filename=$isofile`\
`   initrd /initrd.gz`\
`}`

#### Elementary OS {#elementary_os}

`menuentry '[loopback]elementaryos-freya-amd64.20150411' {`\
`   set isofile='/boot/iso/elementaryos-freya-amd64.20150411.iso'`\
`   loopback loop $isofile`\
`   linux (loop)/casper/vmlinuz boot=casper iso-scan/filename=$isofile locale=`**`en_US.UTF-8`**\
`   initrd (loop)/casper/initrd.lz`\
`}`

#### Fedora

##### Stock installation medium {#stock_installation_medium_1}

`menuentry '[loopback]Fedora-Workstation-netinst-x86_64-24-1.2' {`\
`   set isofile='/boot/iso/Fedora-Workstation-netinst-x86_64-24-1.2.iso'`\
`   loopback loop $isofile`\
`   linux (loop)/isolinux/vmlinuz inst.stage2=hd:LABEL=Fedora-WS-dvd-x86_64-24 iso-scan/filename=$isofile quiet`\
`   initrd (loop)/isolinux/initrd.img`\
`}`

##### Workstation live medium {#workstation_live_medium}

`menuentry '[loopback]Fedora-Workstation-Live-x86_64-24-1.2' {`\
`   set isofile='/boot/iso/Fedora-Workstation-Live-x86_64-24-1.2.iso'`\
`   loopback loop $isofile`\
`   linux (loop)/isolinux/vmlinuz root=live:CDLABEL=Fedora-WS-Live-24-1-2 iso-scan/filename=$isofile rd.live.image quiet`\
`   initrd (loop)/isolinux/initrd.img`\
`}`

#### Gentoo

`menuentry "[loopback]livedvd-amd64-multilib-20160514" {`\
`   set isofile="/boot/iso/livedvd-amd64-multilib-20160514.iso"`\
`   loopback loop $isofile`\
`   linux (loop)/isolinux/gentoo root=/dev/ram0 init=/linuxrc aufs looptype=squashfs loop=/image.squashfs cdroot isoboot=$isofile vga=`**`791`**` splash=silent,theme:default console=tty0`\
`   initrd (loop)/isolinux/gentoo.xz `\
`}`

#### GParted Live {#gparted_live}

`menuentry "[loopback]gparted-live-0.28.1-1-`**`amd64`**`" {`\
`   set isofile="/boot/iso/gparted-live-0.28.1-1-`**`amd64`**`.iso"`\
`   loopback loop $isofile`\
`   linux (loop)/live/vmlinuz boot=live union=overlay username=user config components quiet noswap noeject toram=filesystem.squashfs ip= nosplash findiso=$isofile`\
`   initrd (loop)/live/initrd.img`\
`}`

`menuentry "[loopback]gparted-live-0.28.1-1-`**`amd64`**` EN_US 1920x1200 SWAP" {`\
`   set isofile="/boot/iso/gparted-live-0.28.1-1-`**`amd64`**`.iso"`\
`   loopback loop $isofile`\
`   linux (loop)/live/vmlinuz boot=live union=overlay username=user config components quiet swapon noeject locales=en_US keyboard-layouts=en xvideomode=1920x1200 gl_batch toram=filesystem.squashfs ip= nosplash findiso=$isofile`\
`   initrd (loop)/live/initrd.img`\
`}`

#### Kali Linux {#kali_linux}

`menuentry "[loopback]kali-linux-1.0.7-`**`amd64`**`" {`\
`   set isofile='/boot/iso/kali-linux-1.0.7-`**`amd64`**`.iso'`\
`   loopback loop $isofile`\
`   linux (loop)/live/vmlinuz boot=live findiso=$isofile noconfig=sudo username=root hostname=kali`\
`   initrd (loop)/live/initrd.img`\
`}`

#### Knoppix

`menuentry "[loopback]KNOPPIX_V7.4.2DVD-2014-09-28-EN" {`\
`       set isofile="/boot/iso/KNOPPIX_V7.4.2DVD-2014-09-28-EN.iso"`\
`       loopback loop $isofile`\
`       linux (loop)/boot/isolinux/linux bootfrom=/dev/sda2$isofile acpi=off keyboard=us language-us lang=us`\
`       initrd (loop)/boot/isolinux/minirt.gz`\
`}`

#### Linux Mint {#linux_mint}

`menuentry "Linux Mint 17.2 Cinnamon LTS RC (x64)" {`\
`   set iso=/boot/iso/linuxmint-17.2-cinnamon-64bit.iso`\
`   loopback loop $iso`\
`   linux (loop)/casper/vmlinuz boot=casper iso-scan/filename=$iso noeject noprompt `\
`   initrd (loop)/casper/initrd.lz`\
`}`

#### openSUSE

openSUSE options:
[\#1](https://en.opensuse.org/SDB:Linuxrc#Parameter_Reference),
[\#2](https://doc.opensuse.org/documentation/leap/startup/html/book.opensuse.startup/cha-boot-parameters.html)

##### Stock installation medium {#stock_installation_medium_2}

`menuentry '[loopback]openSUSE-13.1-DVD-x86_64' {`\
`   set isofile='/boot/iso/openSUSE-13.1-DVD-x86_64.iso'`\
`   loopback loop $isofile`\
`   linux (loop)/boot/x86_64/loader/linux install=hd:$isofile`\
`   initrd (loop)/boot/x86_64/loader/initrd`\
`}`

##### Desktop Live medium {#desktop_live_medium_1}

`menuentry '[loopback]openSUSE-13.1-KDE-Live-x86_64' {`\
`   set isofile='/boot/iso/openSUSE-13.1-KDE-Live-x86_64.iso'`\
`   loopback loop $isofile`\
`   linux (loop)/boot/x86_64/loader/linux isofrom_device=$imgdevpath isofrom_system=$isofile LANG=`**`en_US.UTF-8`**\
`   initrd (loop)/boot/x86_64/loader/initrd`\
`}`

#### Parabola GNU/Linux-libre {#parabola_gnulinux_libre}

`menuentry '[loopback]parabola-2015.07.01-dual.iso' {`\
`   set isofile='/boot/iso/parabola-2015.07.01-dual.iso'`\
`   loopback loop $isofile`\
`   linux (loop)/parabola/boot/`**`x86_64`**`/vmlinuz parabolaisolabel=PARA_`**`201507`**` img_dev=$imgdevpath img_loop=$isofile earlymodules=loop`\
`   initrd (loop)/parabola/boot/`**`x86_64`**`/parabolaiso.img`\
`}`

#### Sabayon

`menuentry '[loopback]Sabayon_Linux_14.05_amd64_KDE' {`\
`   set isofile='/boot/iso/Sabayon_Linux_14.05_amd64_KDE.iso'`\
`   loopback loop $isofile`\
`   linux (loop)/boot/sabayon root=/dev/ram0 aufs cdroot locale=`**`en_US`**` loop=/livecd.squashfs looptype=squashfs isoboot=$isofile`\
`   initrd (loop)/boot/sabayon.igz`\
`}`

#### Slackware Linux {#slackware_linux}

`menuentry '[loopback]slackware64-14.1-install-dvd' {`\
`   set isofile='/boot/iso/slackware64-14.1-install-dvd.iso'`\
`   loopback loop $isofile`\
`   linux (loop)/kernels/huge.s/bzImage printk.time=0`\
`   initrd (loop)/isolinux/initrd.img`\
`}`

#### SystemRescueCD

[SystemRescueCD boot
options](https://www.system-rescue-cd.org/manual/Booting_SystemRescueCd/)

`menuentry '[loopback]systemrescuecd-x86-4.5.2' {`\
`   set isofile='/boot/iso/systemrescuecd-x86-4.5.2.iso'`\
`   loopback loop $isofile`\
`   linux (loop)/isolinux/rescue`**`64`**` isoloop=$isofile`\
`   initrd (loop)/isolinux/initram.igz`\
`}`

#### Slitaz

`menuentry 'slitaz-4.0 core' {`\
`   set dir='/live/slitaz-4.0'`\
`   set root=(hd0,msdos3)`\
`   set lang='pt_BR'`\
`   set kmap='br-abnt2'`\
`   linux ($root)/$dir/bzImage lang=$lang kmap=$kmap rw root=/dev/null vga=normal autologin`\
`   initrd ($root)/$dir/rootfs4.gz ($root)/$dir/rootfs3.gz ($root)/$dir/rootfs2.gz ($root)/$dir/rootfs1.gz`\
`}`

#### Slax

`menuentry 'slax' {`\
`   set dir=/live/slax`\
`   set root=(hd0,msdos3)`\
`   linux $dir/boot/vmlinuz from=$dir vga=normal load_ramdisk=1 prompt_ramdisk=0 printk.time=0 slax.flags=perch,xmode`\
`   initrd $dir/boot/initrfs.img`\
`}`

#### Spinrite

`menuentry "Spinrite" {`\
`   set gfxpayload=text`\
`   set isofile="/boot/iso/spinrite.iso"`\
`   set memdisk="/boot/iso/memdisk4.05"`\
`   linux16 (hd1,gpt3)$memdisk iso`\
`   initrd16 (hd1,gpt3)$isofile`\
`}`

#### Tails

`menuentry "[loopback]tails-i386-1.5.iso" {`\
`   set isofile='/boot/iso/tails-i386-1.5.iso'`\
`   loopback loop $isofile`\
`   linux (loop)/live/vmlinuz2 boot=live config findiso=${isofile} live-media=removable apparmor=1 security=apparmor nopersistent noprompt timezone=Etc/UTC block.events_dfl_poll_msecs=1000 noautologin module=Tails`\
`   initrd (loop)/live/initrd2.img`\
`}`

#### Ubuntu

[Casper boot
options](http://manpages.ubuntu.com/manpages/bionic/man7/casper.7.html)

`menuentry '[loopback]ubuntu-14.04.1-desktop-amd64' {`\
`   set isofile='/boot/iso/ubuntu-14.04.1-desktop-amd64.iso'`\
`   loopback loop $isofile`\
`   linux (loop)/casper/vmlinuz.efi boot=casper iso-scan/filename=$isofile locale=`**`en_US.UTF-8`**\
`   initrd (loop)/casper/initrd.lz`\
`}`

#### Xubuntu (32 bit) {#xubuntu_32_bit}

`menuentry '[loopback]Xubuntu-16.04-desktop-i386' {`\
`   set isofile='/boot/iso/xubuntu-16.04-desktop-i386.iso'`\
`   loopback loop $isofile`\
`   linux   (loop)/casper/vmlinuz  file=/cdrom/preseed/xubuntu.seed boot=casper iso-scan/filename=$isofile quiet splash ---`\
`   initrd  (loop)/casper/initrd.lz`\
`}`

#### Qubes Installation image {#qubes_installation_image}

Unpack iso to eg. /live/qubes, pendrive in this example has
\"MultibootUSB\" label

`menuentry 'Qubes extracted to /live/qubes/ of pendrive with MultibootUSB label' {`\
`  multiboot /live/qubes/isolinux/xen.gz placeholder`\
`  module /live/qubes/isolinux/vmlinuz inst.stage2=hd:LABEL=MultibootUSB:/live/qubes rootfstype=vfat rhgb placeholder`\
`  module /live/qubes/isolinux/initrd.img`\
`}`

#### Hyperbola live image {#hyperbola_live_image}

Copy contents of /hyperbola from iso to /live/hyperbola/ on pendrive

`menuentry "Hyperbola (32-bit)" {`\
`   linux /live/hyperbola/boot/i686/vmlinuz hyperisobasedir=live/hyperbola hyperisolabel=MultibootUSB`\
`   initrd /live/hyperbola/boot/i686/hyperiso.img`\
`}`\
\
`menuentry "Hyperbola (64-bit)" {`\
`   linux /live/hyperbola/boot/x86_64/vmlinuz hyperisobasedir=live/hyperbola hyperisolabel=MultibootUSB`\
`   initrd /live/hyperbola/boot/x86_64/hyperiso.img`\
`}`

Chainloading Windows {#chainloading_windows}
--------------------

`menuentry '[chain]en_windows_8.1_professional_x64' {`\
`   insmod chain`\
`       chainloader /efi/boot/bootx64.efi.windows`\
`}`

### See also {#see_also}

Historical ArchWiki entry
<https://wiki.archlinux.org/index.php?title=Multiboot_USB_drive&oldid=479923>
